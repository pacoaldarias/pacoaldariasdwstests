﻿// T5 POO
//1 
Se define como instancia de una clase a ...
{
~Una herencia de una clase
~Una variable
~Una función
=Un objeto
}


//2
Que es incorrecto en las propiedades estáticas de un objeto.
{
~Se puede acceder sin instanciar un objeto
=Sólo puede ser accedida a través de un objeto.
~Su valor es compartido por todos los objetos de la clase
~Se pone la palabra static delante de la variable.
}


//3
Indica cual accede a la variable del  objeto
{
~ print self::$variable
~ print parent::$variable
= print objeto->$variable
~ print objeto.$variable
}




//4
Indica cual accede a la variable estática del  objeto
{
= print self::$variable
~ print parent::$variable
~ print objeto->$variable
~ print objeto.$variable
}






//5
Indica cual accede a la variable heredada  del  objeto
{
~ print self::$variable
= print parent::$variable
~ print objeto->$variable
~ print objeto.$variable
}




//6
Indica cual accede a la variable que hace referencia al objeto
{
~ print self::$variable
~ print parent::$variable
= print this->$variable
~ print objeto->$variable
}




//7
En php qué tipo de visibilidad tienen las constantes.
{
=public
~protected
~static
~private
}


//8
Que es incorrecto en las clases abstractas.
{
~Se pone la palabra abstract delante del nombre
=Pueden instanciarse
~Pueden heredarse
~Contiene métodos abstractos
}


//9
Qué palabra pone en la clase para expresar la herencia de otra {
=extend
~include 
~heritage
~hereda
}




//10
Como se llama la clase que no puede heredar de otra.
{
~const
=final
~fixed
~abstract
}


//11
Las propiedades privadas puede ser accedida por
{
~las funciones de la clase
~los objetos de la clase
= la clase que lo definió.
~ las clase que las heredan.
}


//12
Las propiedades públicas puede ser accedida por
{
~cualquier propiedad de la clase
~cualquier objeto de la clase
~cualquier función de otra clase
=cualquier clase de objeto
}


//13
Qué es incorrecto cuando hablamos del constructor de la clase?
{
~Se llama cuando se crea un objeto
~Lleva el nombre de construct__
=Puede haber más de uno
~Es un método mágico
}


//14
Si definimos en un fichero diferente a la clase persona y queremos crear un objeto persona en otro fichero, debemos de usar:
{
~exclude
=include_once
~extend
~exclude
}


//15
Si queremos que una propiedad de una clase sólo sea accesible desde la misma clase y desde la que hereda debemos declararla como:
{
~private
=protected
~extend
~exclude
}




//16
Si queremos que una propiedad de una clase sólo sea accesible desde la misma clase debemos declararla como:
{
=private
~protected
~public
~exclude
}




//17
Cuando no ponemos ningún tipo de nivel de acceso a las propiedades de una clase, estas son por defecto:
{
~private
~protected
=public
~exclude
}




//18
Como se llama la propiedad de las funciones que deben ser definidas por las clases que las heredan.
{
~extend
~protected
=abstract
~exclude
}


//19
Si tenemos una clase Config para constantes con la variable estática $titulo, como podemos usarlas fuera de esta clase dichas variables.
{
~ Config->titulo
~ Config->::Titulo
= Config::$titulo
~ Config::titulo
}


//20
Si tenemos la clase Persona con el atributo public nombre, como se puede imprimir la variable en otra clase.
{
= print $Persona->nombre;
~ print Persona->$nombre;
~ print $Persona::$nombre;
~ print Persona:->$nombre;
}
