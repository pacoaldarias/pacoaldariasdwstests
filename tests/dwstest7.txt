﻿// T7 SESIONES
//1  
Cual es el objetivo fundamental de las sesiones ...
{
~guardar información de la aplicación
~anotar los accesos
~validar usuarios
=guardar información de los usuarios
}
 
 
//2 Como se llama cuando un cliente se identifica en el servidor, el servidor envía al cliente un código único de identificación y gestión de sesión.
{
~cookie
=sid
~session
~ninguna de las que se muestran.
}
  
//3 
Para establecer una sesión entre el servidor de una aplicación web y el cliente, se puede desarrollar un mecanismo de control mediante el uso de ...


{
~ Sesiones y URL
~ Cookies y Sesiones 
= Cookies y URL
~ Ninguna de las anteriores
}
 
//4
Que es incorrecto en las cookies  ...
{
= el cookie por defecto caduca a la hora
~ requiere recargar la página para poder obtener el cookie.
~ guarda información en el ordenador del usuario
~ la información la genera el servidor
}
 
 
 
 
 
 
//5
Qué función se usa para crear un cookie...
{
= setcookie()
~ createcookie()
~ initcookie()
~ startcookie()
}
 
 
 
 
//6
Que variable del sistema permite acceder al contenido de la cookie ...
{
=$_COOKIE
~$_COOK
~$_SYSTEM
~$COOK
}
  
//7                           
Cual es cierta  ...
{
~Todas son falsas. 
~Las no están relacionadas con un servidor web 
~Las cookies están relacionadas con una página
=Las cookies  están relacionadas con un determinado dominio.
}
  
//8                                   
Cual es falsa
{
~Php incluye los valores de las cookies en la matriz $_REQUEST
~si el nombre de una cookie coincide con el nombre de una entrada en un formulario, en $_REQUEST sólo se guardará el valor de la cookie, no el del control.
=si el nombre de una cookie coincide con el nombre de una entrada en un formulario, en $_REQUEST no se guardará el valor de la cookie, sino el del control.
~ Hay que utilizar la función setcookie() antes de empezar a escribir el contenido de la página.
}


//9
Indica cual es cierta                                   


= La creación de cookies tiene límites, pero cada navegador tiene un límite distinto.
~ Las cookies no deben enviarse antes que cualquier otra cabecera de HTML porque se envían en las cabeceras de las transacciones de HTTP.
~ El límite de cookies  por dominio suele ser de 20 cookies (si se hacen más, no se borran las más antiguas)
~En las cookies el límite de tamaño del valor almacenado suele ser de 4096 Megabytes 
}




//10
La forma más segura de borrar una cookie seria ...
{
~rmcookie("nombre");
~delcookie("nombre");
=setcookie("nombre", "Pepito Conejo", time()-60);
~setcookie("nombre");
}


//11
Bloquear las cookies en el navegador bloquea...
{
~ sesiones
~ cookies
= todas las anteriores


}


//12
La función session_start() que es falso …
{
~inicializa una sesión. 
~y le asigna un SID
~Si no existe la crea y si existe la recupera
= no carga todas las variables de la sessión
}




//13
Como se guarda un valor en una variables de la sesión.
{
~setsession(“nombre”,valor)
= $_SESSION[“nombre”]=valor;
~$$_SESSION[“nombre”]=>valor;
~todas son falsas
}