﻿// Test Tema 3 Ficheros


//1 Es opendir/closedir
¿En PHP qué funciones usamos para abrir y cerrar directorios?
{
~opendir(), readdir()
~opendir(), fclose()
~fgets(), fclose()
=Ninguna de las anteriores
}


//2
En PHP, la función unlink() se emplea para ?
{
~Crear un fichero normal
~Crear un enlace simbólico entre dos         ficheros 
=Borrar un fichero
~Quitar los hipervínculos de un fichero
}


//3
La función file_exists() devuelve true cuando el fichero no existe.
{
~ Verdadero
= Falso
}




//4
Si queremos obtener el tamaño de un fichero en bytes usaremos la función de PHP.
{
~file()
~fsize()
~f_size()
=filesize()
}




//5
Si queremos saber si es final de un fichero usaremos la función de PHP:
{
~end()
~fileend()
~eof()
=feof()
}


//6
Si queremos de un fichero una cadena hasta el final de línea (retorno de carro).
{
=fgets()
~file_gets()
~file()
~filegets() 
}


//7
¿Qué obtendremos al ejecutar file("Ej7.txt") suponiendo que el Ej7.txt existe y tiene contenido? 
{
~Una cadena que contiene todo el contenido del fichero
~Un puntero a fichero que se puede emplear en otras funciones para leer el contenido del fichero.
=Un array en el que cada posición es una  línea del fichero 
~Error
}


//8
¿Cómo se abre un fichero para solo lectura en PHP?
{
~open("datos.txt","read"); 
=fopen("datos.txt","r"); 
~fopen("datos.txt","read"); 
~open("datos.txt"); 
}


//9
Si queremos escribir en un fichero directamente sin tener que abrirlo usaremos la función de PHP:
{
=file_put_contents()
~fwrite() 
~fput()
~Ninguna de las anteriores
}




//10
Para leer de un fichero abierto un número de bytes, usamos la         función de PHP:
{
~fscanf()
=fread()
~fgets()
~fprintf()
}


//11
¿Qué función de PHP lee línea a línea un fichero abierto?
{
~fread()
~fscanf()
=fgets()
~fget()
}


//12
El apuntador de un fichero es la posición en la que leemos o escribimos en él. 
{
=Verdadero
~Falso
}




//13
¿Qué problema puede darse con este código fopen(“Programa.php”,”r”); ?
{
=Que el fichero a leer no exista y dé error. Podemos solucionarlo con un bucle if viendo si fopen devuelve         false.
~Que el fichero a leer no exista y dé error. Podemos solucionarlo con un bucle if viendo si fopen devuelve true.
~ Que el fichero a leer no exista y dé error.         Podemos solucionarlo poniendo @ delante de la instrucción if y usando die.
~Ninguna de las anteriores
}


//14
¿Qué ocurre si no cierro un archivo que he abierto en PHP?
{
~Nada, al cerrar el script el archivo se cerrará también.
=Que quedará bloqueado por el sistema operativo y no podrán acceder a él otros programas.
~Que cuando cierre el script perderé todos los cambios.
~Que perderé prioridades para leer el archive.
}




//15
La función fopen() de PHP no:
{
~Permite abrir rutas relativas y absolutas
~Permite abrir ficheros de otros servidores
~Permite usar \ y / para separar los directorios
=Permite cerrar el fichero
}




//17
La función die() de PHP se encarga de escribir un mensaje por         pantalla pero deja abierto el script:
{
~Verdadero
=Falso
}


//18
La función de PHP unlink() no podrá llevar a cabo su cometido         cuando:
{
~La imagen que está hipervinculada no exista.
=El fichero no exista.
~El directorio esté dentro del servidor.
~Ninguna de las anteriores.
}


//19
Si queremos  el archivo en forma de array donde cada elemento del array es una línea según el separador que se le haya pasado como parámetro.
{
~fgets()
~file_gets()
=file()
~filegets() 
}