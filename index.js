var btn = document.getElementById('cmdRevise')

btn.onclick = function(){
    var n = 0 
    if (btn.value == 'Corregir') {
        var trues = document.getElementsByClassName('cls_true')
        var falses = document.getElementsByClassName('cls_false')
        
        for (let i=0; i<falses.length; i++) 
            falses[i].style.color="red"

        for (var i=0; i<trues.length; i++) {
            trues[i].style.color="green"
            var input = trues[i].querySelector('input')

            n += input.checked 
        }

        btn.value = 'Reset'
        
        alert(n + ' aciertos de ' + i + ' preguntas' )

    } else {
        var span = document.getElementsByTagName("span")
        for (var i=0; i<span.length; i++) 
            span[i].style.color="black"

        btn.value = 'Corregir'
    }
}
